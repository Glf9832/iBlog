#_*_coding:utf8_*_


class Config(object):
    SECRET_KEY = 'hard to guess'


class ProdConfig(Config):
    SQLALCHEMY_DATABASE_URI = "mysql+mysqldb://root:123456@localhost:3306/iBlog"
    SQLALCHEMY_ECHO = False
    SQLALCHEMY_TRACK_MODIFICATIONS = False


class DevConfig(Config):
    DEBUG = True
    # SQLALCHEMY_DATABASE_URI = "sqlite:///iBlog.db"
    SQLALCHEMY_DATABASE_URI = "mysql+mysqldb://root:mchip.13579@localhost:3306/iBlog"
    SQLALCHEMY_ECHO = False
    SQLALCHEMY_TRACK_MODIFICATIONS = False
